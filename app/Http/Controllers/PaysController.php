<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\pays;
class PaysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.liste',[
            "pays"=> Pays::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.formulaire');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // validation de la requete
        $request->validate([
            "libelle"=>'required',
            "capital"=>'required',
            "continent"=>'required'

        ]);
        pays::create([
            "libelle" => $request->get('libelle'),
            "description" => $request->get('description'),
            "code_indicatif" => $request->get('code_indicatif'),
            "continent" => $request->get('continent'),
            "population" => $request->get('population'),
            "capital" => $request->get('capital'),
            "monnaie" => $request->get('monnaie'),
            "superficie" => $request->get('superficie'),
            "etre_laique" => $request->get('etre_laique'),

        ]);

        return redirect()-> route("layout.main");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\pays  $pays
     * @return \Illuminate\Http\Response
     */
    public function show(pays $pays)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\pays  $produit
     * @return \Illuminate\Http\Response
     */
    public function edit(pays $pays)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\pays  $pays
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, pays $pays)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\pays  $pays
     * @return \Illuminate\Http\Response
     */
    public function destroy(pays $pays)
    {
        //
    }
}
