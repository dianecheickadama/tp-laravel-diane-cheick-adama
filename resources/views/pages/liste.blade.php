@extends('layout.main')

@section('content')

  <div class="col-md-12">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title ">Liste des Pays</h4>
        <p class="card-category"> Voici la liste</p>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead>
                <tr>
                  <th>ID</th>
                  <th>Libelle</th>
                  <th>Description</th>
                  <th>Code</th>
                  <th>Continent</th>
                  <th>Population</th>
                  <th>Capital</th>
                  <th>Monnaie</th>
                  <th>Superficie</th>
                  <th>Laique?</th>

                </tr>
              </thead>
              <tbody>
                @foreach ($pays as $Lepays)
                  <tr>
                      <td>{{$Lepays->id}}</td>
                      <td>{{$Lepays->libelle}}</td>
                      <td>{{$Lepays->description}}</td>
                      <td>{{$Lepays->code_indicatif}}</td>
                      <td>{{$Lepays->continent}}</td>
                      <td>{{$Lepays->population}}</td>
                      <td>{{$Lepays->capital}}</td>
                      <td>{{$Lepays->monnaie}}</td>
                      <td>{{$Lepays->superficie}}</td>
                      <td>{{$Lepays->etre_laique}}</td>
                  </tr>
                @endforeach
              </tbody>
          </table>
          </div>
      </div>
    </div>
  </div>
@endsection
