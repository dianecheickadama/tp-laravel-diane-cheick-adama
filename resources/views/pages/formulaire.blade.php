@extends('layout.main')

@section('content')

<section class="content">
    <div >
        <div class="row">
            <div class="col-md-9" style="margin-left: auto;margin-right: auto">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Ajout de Pays</h3>
                    </div>
                    <form class="form-horizontal" action="{{route('pays.store')}}" method="POST">
                        @method("POST")
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label>Libélé</label>
                                <input name="libelle" type="text" class="form-control" placeholder="Ex: Côte dùIvoire">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Description</label>
                                <textarea name="description" type="text" class="form-control" placeholder="Pays de l'afrique de l'ouest faisant frontière avec le Mali, le burkina, le Ghana, le liberia et la ghuinée. Agréable destination touristique, la cote d'ivoire est aussi un ou règne l'entrepreneuriat"></textarea>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Code Indicatif</label>
                                <input name="code_indicatif" type="text" class="form-control" placeholder="+225">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Continent</label>
                                <input name="continent"  type="text" class="form-control" placeholder="Afrique">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Population</label>
                                <input name="population" type="text" class="form-control" placeholder="25 000 000 ">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Capital</label>
                                <input name="capital" type="text" class="form-control" placeholder="Abidjan">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Monnaie</label>
                                <input name="monnaie" type="text" class="form-control" placeholder="XOF; Fcfa">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Superficie (en Km2)</label>
                                <input name="superficie" type="text" class="form-control" placeholder="322 462 ">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Pays Laique ? OUI:NON</label>
                                <input name="etre_laique" type="text" class="form-control" placeholder="OUI">
                            </div>
                        </div>


                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Ajouter</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
