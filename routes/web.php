<?php

use App\Http\Controllers\PaysController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layout.main');
})->name('layout.main');
Route::get('/PaysListe',[PaysController::class ,'index']);
Route::get('/CreePays',[PaysController::class ,'create']);
Route::post('/EnregProduit',[PaysController::class ,'store'])->name('pays.store');

