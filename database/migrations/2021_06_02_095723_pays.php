<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        schema::create('pays',function(Blueprint $table){
            $table->id();
            $table->string('libelle');
            $table->string('description')->nullable()->default('Ceci est un pays');
            $table->string('code_indicatif')->nullable();
            $table->string('continent');
            $table->integer('population')->nullable()->default(20000000);;
            $table->string('capital');
            $table->string('monnaie')->nullable();
            $table->integer('superficie')->nullable();
            $table->boolean('etre_laique')->nullable()->default(true);
            $table->timestamps();
        });}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
