<?php

namespace Database\Factories;

use App\Models\Pays;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaysFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Pays::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'libelle'=>$this->faker->country,
            'description'=>$this->faker->sentence,
            'code_indicatif'=>$this->faker->randomElement(['+225', '+226','+142','+233']),
            'continent'=>$this->faker->randomElement(['Afrique','Amerique','Europe','Asie','Oceanie']),
            'population'=>$this->faker->randomElement([2000000,3000000,1000000,22000000]),
            'capital'=>$this->faker->randomElement(['Abidjan','Bamako','lagos','new york', 'tunis','ouagadougou','paris']),
            'monnaie'=>$this->faker->randomElement(['XOF','EUR','DOLLAR']),
            'superficie'=>$this->faker->randomElement([2000000,3000000,1000000,22000000]),
            'etre_laique'=>$this->faker->boolean,
        ];
    }
}
